# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 15:09:26 2020

@author: Diego D. Navarro
"""
import json
from difflib import get_close_matches
DATA = json.load(open('dic/data.json'))
#DATA = json.load(open('/home/diegodn/Documents/Kivi-tech_with_tim/Dict/dic/data.json'))

def check(word):
    return word in DATA

def defi(word):
    '''
    Parameters
    ----------
    word : String
        Word to be described.

    Returns
    -------
    word : String
        Word with 1 or more definitions.

    '''
    if word in DATA:
        word = meaning(word)
    elif len(get_close_matches(word, DATA.keys())) > 0:
        word = match(word)
    else:
        word = 'The word does not exists.'
    return word

def meaning(word):
    '''
    Parameters
    ----------
    word : String
        Word to be described.

    Returns
    -------
    word : String
        Word with 1 or more definitions.

    '''
    s_s = '{}\n'.format(word.capitalize())
    w_m = DATA[word]
    for i, k in enumerate(w_m):
        s_s = s_s + '{}. {}\n'.format(i+1, k)
    return s_s

def match(word):
    '''
    Parameters
    ----------
    word : String
        Word wrongfully written to be matched with close words.

    Returns
    -------
    word : String
        Word with 1 or more definitions.

    '''
    matches = get_close_matches(word, DATA.keys(), cutoff=0.7)
    s_s = 'Did you mean any of these?\n'
    for i in matches:
        s_s = s_s + '{}\n'.format(i.capitalize())
    s_s = s_s + 'Type new word.\n'
    new_word = input(s_s)
    word = meaning(new_word)
    return word

def match_word(word):
    '''
    Parameters
    ----------
    word : String
        Word wrongfully written to be matched with close words.

    Returns
    -------
    word : String
        List of strings.

    '''
    matches = get_close_matches(word, DATA.keys())
    print(matches)
    return matches

def matching(word):
    if len(get_close_matches(word, DATA.keys(), cutoff=0.7)) > 0:
        return True
    else:
        return False
