# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 13:59:46 2020

@author: Diego D. Navarro
"""
import dic_def
'''
while True:
    WORD = input('Give me a word: ')
    print(dic_def.defi(WORD.lower()))
    ANS = input('Do you wish to continue?\n(y/n): ')
    if ANS == 'n':
        print('Goodbye.')
        break
''' 
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.properties import ObjectProperty

class MyGrid(GridLayout):
    name = ObjectProperty(None)

    def btn(self):
        WORD = self.name.text
        self.print_txt.text = "Name: {}".format(dic_def.defi(WORD.lower()))

class MyApp(App):
    def build(self):
        return MyGrid()
    
if __name__=="__main__":
    MyApp().run()