# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 13:59:46 2020

@author: Diego D. Navarro
"""
import dic_def
#from kivy.app import App
from kivy.core.text import LabelBase
from kivymd.app import MDApp, App
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.properties import ObjectProperty
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.popup import Popup
from kivy.core.window import Window


wrd = "None"
lst_op = ["", "", ""]

class InputWindow(Screen):
    word = ObjectProperty(None)
    Window.size = (1125/3, 2436/3)
    def btn(self):
        global wrd
        global lst_op
        wrd = self.word.text
        chck = dic_def.check(wrd.lower())
        if chck:
            sm.current = "def_win"
            self.word.text = ""
        else:
            mt = dic_def.matching(wrd.lower())
            if mt:
                lst_op = dic_def.match_word(wrd.lower())
                pop().open()
            else:
                err().open()


class DefWindow(Screen):
    defi = ObjectProperty(None)
    defi = dic_def.meaning(wrd.lower())
    btnC = ObjectProperty(None)

    def on_enter(self, *args):
        self.btnC.text = "Copy Text"
        definition = dic_def.meaning(wrd.lower())
        self.defi.text = definition

    def copy(self, x):
        import pyperclip as ctrl
        ctrl.copy(x)
        self.btnC.text = "Copied!"


class WindowManager(ScreenManager):
    pass


class pop(Popup):
    op1 = ObjectProperty(None)
    op2 = ObjectProperty(None)
    op3 = ObjectProperty(None)
    print(lst_op)

    def on_open(self):
        self.op1.text = lst_op[0]
        self.op2.text = lst_op[1]
        self.op3.text = lst_op[2]

    def btn1(self):
        global wrd
        wrd = lst_op[0]
        sm.current = "def_win"

    def btn2(self):
        global wrd
        wrd = lst_op[1]
        sm.current = "def_win"

    def btn3(self):
        global wrd
        wrd = lst_op[2]
        sm.current = "def_win"


class err(Popup):
    pass


kv = Builder.load_file("design.kv")

sm = WindowManager()

screens = [InputWindow(name="i_word"), DefWindow(name="def_win")]
for screen in screens:
    sm.add_widget(screen)

sm.current = "i_word"


class MyApp(App):
    def build(self):
        return sm

if __name__ == "__main__":
    MyApp().run()
